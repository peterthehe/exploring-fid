# Exploring FID

Based on [this](https://github.com/mseitzer/pytorch-fid).

To run:

`python3 fid_score.py /path/real /path/fake --num-workers=0 --num-filters=10`

where `/path/real` and `/path/fake` are directories containing images.

If you just want to visualise feature distributions, just let the two paths be equal.
